# Виведіть із списку чисел список квадратів парних чисел.
# Використовуйте 2 варіанти рішення: генератор та цикл


# Генератор
def squares_of_even(lst):
    """Генератор, який повертає список квадратів парних чисел"""
    return (x**2 for x in lst if x % 2 == 0)


numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
squares = squares_of_even(numbers)
print(list(squares))  # Конвертація генератора у список і друк списку

#def square_even_numbers(numbers):
# """Генератор"""
   # for num in numbers:
       # if num % 2 == 0:
        #    yield num ** 2

# numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# even_squares = list(square_even_numbers(numbers))
# print(even_squares)


# Цикл
def squares_of_even(lst):
    """
    Функція, яка повертає список квадратів парних чисел, використовуючи цикл
    """
    result = []
    for x in lst:
        if x % 2 == 0:
            result.append(x**2)
    return result


numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
squares = squares_of_even(numbers)
print(squares)
